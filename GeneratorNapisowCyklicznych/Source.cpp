#include <stdio.h>
#include <iostream>
using namespace std;

struct kontener {
	kontener *nastepny;
	kontener *poprzedni;
	char zawartosc;
};


class ListaCykliczna {
public:
	kontener *poczatekListy;
	kontener *koniecListy;

	ListaCykliczna();
	bool jestPusta();
	void dodaj(char dane);
	void przesun(int ile);
	void wypisz();
	void usun(int ile);
	void odwroc();
	void zeruj();
};

ListaCykliczna::ListaCykliczna() {
	poczatekListy = NULL;
	koniecListy = NULL;
	//kierunek = true;
}
bool ListaCykliczna::jestPusta() {
	if (poczatekListy == NULL) {
		return true;
	}
	return false;
}
void ListaCykliczna::dodaj(char dane) {
	kontener *nowyElement = new kontener();
	nowyElement->zawartosc = dane;

	if (jestPusta()) {
		nowyElement->nastepny = nowyElement;
		nowyElement->poprzedni = nowyElement;

		poczatekListy = nowyElement;
		koniecListy = nowyElement;
	}
	else {
		koniecListy->nastepny = nowyElement;
		nowyElement->poprzedni = koniecListy;
		nowyElement->nastepny = poczatekListy;
		poczatekListy->poprzedni = nowyElement;
		koniecListy = nowyElement;
	}
}
void ListaCykliczna::wypisz() {
	cout << "WYPISUJE" << endl;
	if (poczatekListy != NULL) {
		kontener *tmp = poczatekListy;
		while (tmp != koniecListy)
		{
			cout << tmp->zawartosc;
			tmp = tmp->nastepny;
		}
		cout << tmp->zawartosc << endl;
	}
	else {
		cout << "Lista jest pusta" << endl;
	}
	return;
}
void ListaCykliczna::przesun(int ile) {
	for (int i = 0; i < ile; ++i) {
		poczatekListy = poczatekListy->nastepny;
		koniecListy = koniecListy->nastepny;
	}
}
void ListaCykliczna::usun(int ile) {
	kontener *tmp = poczatekListy;
	for (int i = 0; i < ile; ++i) {
		if (tmp == koniecListy) {
			delete poczatekListy;
			poczatekListy = NULL;
			koniecListy = NULL;
			return;
		}
		else {
			tmp = poczatekListy;
			tmp = tmp->nastepny;
			delete poczatekListy;
			poczatekListy = tmp;
		}
	}
	koniecListy->nastepny = poczatekListy;
}
void ListaCykliczna::odwroc() {
	//odwrocenie wewnetrzynch wskaznikow listy 
	kontener *tmp = poczatekListy;
	kontener *tmp2 = poczatekListy->nastepny;

	if (poczatekListy != koniecListy) {
		do {
			tmp->nastepny = tmp->poprzedni;
			tmp->poprzedni = tmp2->nastepny;

			tmp = tmp2;
			tmp2 = tmp2->nastepny;
		} while (tmp != koniecListy);

		tmp->nastepny = tmp->poprzedni;
		tmp->poprzedni = tmp2->nastepny;
	}

	//odwrocenie poczatkowych
	tmp = poczatekListy;
	poczatekListy = koniecListy;
	koniecListy = tmp;
}
void ListaCykliczna::zeruj() {
	if (poczatekListy != NULL) {
		kontener *tmp = poczatekListy;
		kontener *tmp2 = poczatekListy->nastepny;

		while (tmp != koniecListy)
		{
			delete tmp;
			tmp = tmp2;
			tmp2 = tmp2->nastepny;
		}
		delete tmp;
		poczatekListy = NULL;
		koniecListy = NULL;
		//cout << tmp->zawartosc << endl;
	}
}

int main() {
	char komenda[8] = "";

	char rejestr;
	cout << "podaj komende " << endl;
	cin >> komenda;
	cin >> rejestr;

	ListaCykliczna lista;

	system("pause");
	return 0;
}